package com.example.bmicalculator;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.DecimalFormat;

public class Controller {

    @FXML
    public Label labelBMI, labelStatus;

    @FXML
    public TextField age,kg,heightMeter;

    @FXML
    public RadioButton male,female;

    private static final DecimalFormat df = new DecimalFormat("0.00");

    @FXML
    public void clear1(){
        age.clear();
        kg.clear();
        heightMeter.clear();
        labelBMI.setText("");
        labelStatus.setText("");
    }

    @FXML
    public void calculateBMI() {
        String temp1 = kg.getText();
        double mass = Integer.parseInt(temp1);
        String temp2 = heightMeter.getText();
        double height = Integer.parseInt(temp2);

        height = height/100;
        double temp7 = Math.pow(height,2);
        double bmi = mass / temp7;

        status(bmi);
        String temp3 = df.format(bmi);;
        labelBMI.setText(temp3);
    }

    @FXML
    private void status(double temp){
        if (temp > 0 && temp < 16){
            labelStatus.setText("Severe Thinness");
        }
        else if (temp >= 16 && temp < 17){
            labelStatus.setText("Moderate Thinness");
        }
        else if (temp >= 17 && temp < 18.50){
            labelStatus.setText("Mild Thinness");
        }
        else if (temp >= 18.5 && temp < 25){
            labelStatus.setText("Normal");
        }
        else if (temp >= 25 && temp < 30){
            labelStatus.setText("Overweight");
        }
        else if (temp >= 30 && temp < 35){
            labelStatus.setText("Obese Class I");
        }
        else if (temp >= 35 && temp < 40){
            labelStatus.setText("Obese Class II");
        }
        else if (temp >= 40){
            labelStatus.setText("Obese Class III");
        }
    }

}